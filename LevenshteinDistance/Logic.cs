﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LevenshteinDistance
{
    class Logic
    {
        private Algorithm algorithm = new Algorithm();
        private const int wordCountInTopList = 10;

        public Dictionary<string, int> SendRequest(String textRequest, TextBox mainText, TextBox topList)
        {
            String text = DeleteAllCharactersExceptLetters(mainText.Text);
            String[] textSplit = text.Split(" ");

            Dictionary<string, int> keywordsAndDistLevenshtein = FillMapKeywordsAndDistLevenshtein(textSplit, textRequest);

            keywordsAndDistLevenshtein = SortByValue(keywordsAndDistLevenshtein);
            keywordsAndDistLevenshtein = TrimDictionary(keywordsAndDistLevenshtein);

            return keywordsAndDistLevenshtein;
        }

        private String DeleteAllCharactersExceptLetters(String text)
        {
            return Regex.Replace(text, "(?i)[^А-ЯЁA-Z]", " ");
        }

        private Dictionary<string, int> FillMapKeywordsAndDistLevenshtein(String[] textSplit, String textRequest)
        {
            Dictionary<string, int> keywordsAndDistLevenshtein = new Dictionary<string, int>();
            foreach (String word in textSplit)
            {
                int dist = algorithm.DistLevenshtein(textRequest, word);
                if (word != "") keywordsAndDistLevenshtein.TryAdd(word, dist);
            }
            return keywordsAndDistLevenshtein;
        }

        private Dictionary<string, int> SortByValue(Dictionary<string, int> dict) 
        {
            return dict = dict.OrderBy(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        private Dictionary<string, int> TrimDictionary(Dictionary<string, int> dict)
        {
            foreach (var obj in dict.Keys.Skip(wordCountInTopList).ToList()) dict.Remove(obj);
            return dict;
        }
    }
}
