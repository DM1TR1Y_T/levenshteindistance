﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Words.NET;

namespace LevenshteinDistance
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Logic logic = new Logic();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnLoadFileBtnClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Выберите документ";
            fileDialog.DefaultExt = ".docx";
            fileDialog.Filter = "Text documents (.txt)|*.txt|Word documents (.docx)|*.docx";
            Nullable<bool> result = fileDialog.ShowDialog();

            if (result == false)
            {
                MessageBox.Show(this, "Файл не выбран!");
                return;
            }

            MainText.Text = String.Empty;

            string ext = System.IO.Path.GetExtension(fileDialog.FileName);

            if (ext == ".txt")
            {
                MainText.Text = File.ReadAllText(fileDialog.FileName, Encoding.Default);
            }
            else if (ext == ".docx")
            {
                using (var document = DocX.Load(fileDialog.FileName))
                {
                    document.GetSections().ToList().ForEach(x => x.SectionParagraphs.ForEach(y => MainText.Text += y.Text + '\n'));
                }
            }
        }

        private void OnSearchBtnClicked(object sender, RoutedEventArgs e)
        {
            string text = MainText.Text;

            if (String.IsNullOrEmpty(text))
            {
                MessageBox.Show("Откройте файл с текстом!");
            } else if (String.IsNullOrEmpty(SearchText.Text))
            {
                MessageBox.Show("Введите слово!");
            }
            else
            {
                Dictionary<string, int> dict = logic.SendRequest(SearchText.Text, MainText, TopList);
                string textTopList = "";

                foreach(var item in dict)
                {
                    textTopList += item.Key + " - " + item.Value + "\n";
                }

                TopList.Text = textTopList;
            }
        }
    }
}
