﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevenshteinDistance
{
    class Algorithm
    {
        public int DistLevenshtein(String s1, String s2)
        {
            if (s1.Equals(s2))
                return 0;

            if (s1.Length == 0)
                return s2.Length;

            if (s2.Length == 0)
                return s1.Length;

            int[,] dp = new int[s1.Length + 1, s2.Length + 1];

            for (int i = 0; i <= s1.Length; i++)
            {
                dp[i, 0] = i;
            }

            for (int i = 0; i <= s2.Length; i++)
            {
                dp[0, i] = i;
            }

            for (int i = 1; i <= s1.Length; i++)
            {
                for (int j = 1; j <= s2.Length; j++)
                {
                    int d1 = dp[i - 1, j] + 1;
                    int d2 = dp[i, j - 1] + 1;
                    int d3 = dp[i - 1, j - 1];
                    if (s1[i - 1] != s2[j - 1])
                    {
                        d3 += 1;
                    }
                    dp[i, j] = Math.Min(Math.Min(d1, d2), d3);
                }
            }
            return dp[s1.Length, s2.Length];
        }
    }
}
